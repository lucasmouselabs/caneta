//Class
public class Caneta {
	//Atributos
	private String cor;
	private String material;
	private Boolean tampa;
	
	//METODOs
	public void setcor(String umaCor){
		cor = umaCor; 
	}
	public String getcor(){
		return cor;
	}
	public void setMaterial (String umMaterial){
		material = umMaterial;
	}
	public String getMaterial(){
		return material;
	}
	public void setTampa (Boolean umatampa){
		tampa = umatampa;
	}
	public String getTampa (){
		if (tampa == true){
			return "com tampa";
		}else{
			return "Sem tampa";
		}
	}
}


